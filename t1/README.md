CI218 - Trabalho
================

Equipe
------

Fernando Aoyagui Shinohata - GRR20165388
Lucas Sampaio Franco - GRR20166836

Sobre o trabalho
----------------

Todo o trabalho foi feito em Python, e está no arquivo `main.py`. O ponto de início é a função `main()`.

O ponto de partida para a verificação e execução dos algoritmos de detecção de conflitos é o método `Schedule::withoutConflicts()`, que retorna uma lista com os escalonamentos e os resultados das execuções dos algoritmos.

Os algoritmos de teste de serialidade por conflito e equivalência por visão são chamados pela classe `ConflictResolver`. O teste de serialidade por conflito utiliza a classe `Graph` para gerar e detectar ciclos no grafo, e o teste de equivalência por visão utiliza a classe `ScheduleInformation` para analisar as regras de equivalência.

Sobre o Makefile
----------------

Como o trabalho foi feito em Python, o arquivo "executável" `escalona` gerado pelo Makefile nada mais é que uma cópia de `main.py` com permissões de execução e com um cabeçalho dizendo qual o programa que o interpreta.
