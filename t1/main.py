#!/usr/bin/python3
import sys
from itertools import permutations

class GraphVertex:
	def __init__(self, _id):
		self.neighbors = SafeList()
		self.id = _id
		self.processed = False

	def addNeighbor(self, neighbor):
		if self.neighbors.index(neighbor) < 0:
			self.neighbors.append(neighbor)


class Graph:
	def __init__(self):
		self.vertexes = []

	def addVertex(self, _id):
		self.vertexes.append(GraphVertex(_id))

	def getVertex(self, _id):
		for vertex in self.vertexes:
			if vertex.id == _id:
				return vertex
		return None

	def addEdge(self, idA, idB):
		vA = self.getVertex(idA)
		vB = self.getVertex(idB)
		vA.addNeighbor(vB)

	def _recursiveHasCycle(self, vertex, stack):
		vertex.processed = True
		stack.append(vertex.id)
		for i in range(0, len(vertex.neighbors)):
			neighbor = vertex.neighbors[i]
			if (neighbor.processed == True and neighbor.id in stack) or (neighbor.processed == False and self._recursiveHasCycle(neighbor, stack)):
				return True
		stack.remove(vertex.id)
		return False

	def hasCycle(self):
		for vertex in self.vertexes:
			vertex.processed = False
		for vertex in self.vertexes:
			if self._recursiveHasCycle(vertex, []) == True:
				return True
		return False

class ScheduleInformation:
	def __init__(self, operations):
		self.operations = []
		self.writeOperations = []
		self.readOperations = []
		self.lastWriteOperation = None
		for operation in operations:
			self.appendOperation(operation)
		self.lastWriteOperation = self.writeOperations[-1]
		self.readsAfterWrites = []
		self.loadReadsAfterWrites()
	
	def loadReadsAfterWrites(self):
		for readOperation in self.readOperations:
			for writeOperation in self.writeOperations:
				if writeOperation.timestamp < readOperation.timestamp and writeOperation.attribute == readOperation.attribute and writeOperation.transactionId != readOperation.transactionId:
					self.readsAfterWrites.append((writeOperation, readOperation))

	@staticmethod
	def fromTransactionList(transactionList, sort = False, retainTimestamps = False):
		operations = []
		if retainTimestamps:
			for transaction in transactionList:
				for operation in transaction.operations:
					operations.append(operation)
		else:
			timestamp = 1
			for transaction in transactionList:
				for operation in transaction.operations:
					operations.append(Operation(timestamp, operation.transactionId, operation.operation, operation.attribute))
					timestamp += 1
		if sort:
			operations.sort(key=lambda op: op.timestamp)
		return ScheduleInformation(operations);

	def readsAfterWritesAreEqualTo(self, otherScheduleInformation):
		if len(self.readsAfterWrites) != len(otherScheduleInformation.readsAfterWrites):
			return False

		for (write, read) in self.readsAfterWrites:
			hasThisTuple = False
			for (otherWrite, otherRead) in otherScheduleInformation.readsAfterWrites:
				if write.isEqualTo(otherWrite) and read.isEqualTo(otherRead):
					hasThisTuple = True
			if hasThisTuple == False:
				return False
		return True

	def isEqualTo(self, otherScheduleInformation):
		return (
			self.lastWriteOperation.isEqualTo(otherScheduleInformation.lastWriteOperation)
			and self.readsAfterWritesAreEqualTo(otherScheduleInformation)
		)

	def appendOperation(self, operation):
		if operation.operation == 'R':
			self.readOperations.append(operation)
		elif operation.operation == 'W':
			self.writeOperations.append(operation)
		self.operations.append(operation)

	def __str__(self):
		representation = ''
		representation += 'ScheduleInformation:\n'
		representation += 'last write: {}\n'.format(str(self.lastWriteOperation))
		representation += 'reads after writes:\n'
		for (w, r) in self.readsAfterWrites:
			representation += '{}, {}\n'.format(str(w), str(r))
		representation += 'operations:\n'
		for o in self.operations:
			representation += str(o) + '\n'
		return representation

class ConflictResolver:
	@staticmethod
	def testConflictSerializability(transactionList):
		g = Graph()
		for transaction in transactionList:
			g.addVertex(transaction.id)
		for i in range(0, len(transactionList)):
			for j in range(i+1, len(transactionList)):
				tA = transactionList[i]
				tB = transactionList[j]
				if tB.readsAfterWriteOf(tA) or tB.writesAfterReadOf(tA) or tB.writesAfterWriteOf(tA):
					g.addEdge(tA.id, tB.id)
				if tA.readsAfterWriteOf(tB) or tA.writesAfterReadOf(tB) or tA.writesAfterWriteOf(tB):
					g.addEdge(tB.id, tA.id)
		if g.hasCycle():
			return 'NS'
		return 'SS'

	@staticmethod
	def testViewEquivalence(transactionList):
		scheduleInformation = ScheduleInformation.fromTransactionList(transactionList, sort=True, retainTimestamps=True)

		for executionOrder in permutations(transactionList):
			permutedScheduleInformation = ScheduleInformation.fromTransactionList(executionOrder, sort=False)
			if scheduleInformation.isEqualTo(permutedScheduleInformation):
				return 'SV'
			del permutedScheduleInformation
		return 'NV'

class SafeList(list):
	def index(self, element):
		try:
			return super(SafeList, self).index(element)
		except ValueError:
			return -1

class TransactionList(list):
	def __str__(self):
		transactionIds = []
		for transaction in self:
			transactionIds.append(str(transaction.id))
		transactionIds.sort(key=lambda t: int(t))
		return ','.join(transactionIds)

class Operation:
	def __init__(self, timestamp, transactionId, operation, attribute):
		self.timestamp = timestamp
		self.transactionId = transactionId
		self.operation = operation
		self.attribute = attribute

	def isEqualTo(self, otherOperation):
		return self.transactionId == otherOperation.transactionId and self.operation == otherOperation.operation and self.attribute == otherOperation.attribute

	def __str__(self):
		return 'Operation: ( timestamp: {}, transactionId: {}, operation: {}, attribute: {} )'.format(self.timestamp, self.transactionId, self.operation, self.attribute)

class Transaction:
	OPERATION_SWITCH = {
		'R': lambda t: t.readOperations,
		'W': lambda t: t.writeOperations
	}

	def __init__(self, _id):
		self.operations = []
		self.id = int(_id)
		self.startTime = 0
		self.endTime = 0
		self.readOperations = []
		self.writeOperations = []

	def addOperation(self, operation):
		if type(operation) != Operation:
			raise Exception('Attempt to add an invalid operation to transaction ' + self.id)
		if len(self.operations) == 0:
			self.startTime = int(operation.timestamp)
		self.operations.append(operation)
		if self.endTime < int(operation.timestamp):
			self.endTime = int(operation.timestamp)
		try:
			Transaction.OPERATION_SWITCH.get(operation.operation)(self).append(operation)
		except TypeError:
			pass

	def readsAfterWriteOf(self, otherTransaction):
		for i in range(0, len(self.readOperations)):
			for j in range(0, len(otherTransaction.writeOperations)):
				if self.readOperations[i].attribute == otherTransaction.writeOperations[j].attribute and self.readOperations[i].timestamp > otherTransaction.writeOperations[j].timestamp:
					return True
		return False

	def writesAfterReadOf(self, otherTransaction):
		for i in range(0, len(self.writeOperations)):
			for j in range(0, len(otherTransaction.readOperations)):
				if self.writeOperations[i].attribute == otherTransaction.readOperations[j].attribute and self.writeOperations[i].timestamp > otherTransaction.readOperations[j].timestamp:
					return True
		return False

	def writesAfterWriteOf(self, otherTransaction):
		for i in range(0, len(self.writeOperations)):
			for j in range(0, len(otherTransaction.writeOperations)):
				if self.writeOperations[i].attribute == otherTransaction.writeOperations[j].attribute and self.writeOperations[i].timestamp > otherTransaction.writeOperations[j].timestamp:
					return True
		return False

	def intersectsWith(self, otherTransaction):
		return (self.startTime <= otherTransaction.startTime and otherTransaction.startTime <= self.endTime) or (self.startTime <= otherTransaction.endTime and otherTransaction.endTime <= self.endTime)

	def __str__(self):
		representation = 'Transaction: ( id: {}, startTime: {}, endTime: {}, operations:\n'.format(str(self.id), str(self.startTime), str(self.endTime))
		for operation in self.operations:
			representation += '  ' + str(operation) + '\n'
		representation += ' )'
		return representation

class Schedule:
	def __init__(self):
		self.transactions = {}
		self.operations = []
		self.writeOperations = []
		self.readOperations = []
		self.lastWriteOperation = None

	def appendOperation(self, operation):
		if not Validator.isOperation(operation):
			raise Exception('Attempt to add an invalid operation to schedule')
		try:
			self.transactions[operation.transactionId].addOperation(operation)
		except KeyError:
			self.transactions[operation.transactionId] = Transaction(operation.transactionId)
			self.transactions[operation.transactionId].addOperation(operation)
		self.operations.append(operation)
		if operation.operation == 'R':
			self.readOperations.append(operation)
		if operation.operation == 'W':
			self.writeOperations.append(operation)
			if self.lastWriteOperation == None or self.lastWriteOperation.timestamp < operation.timestamp:
				self.lastWriteOperation = operation

	def withoutConflicts(self):
		transactionsWithIntersection = []
		processedTransactions = SafeList()
		for i in range(1, len(self.transactions) + 1):
			i = str(i)
			if processedTransactions.index(i) < 0:
				processedTransactions.append(i)
				transactionsThatIntersectWithI = TransactionList([self.transactions[i]])
				for j in range(1, len(self.transactions) + 1):
					j = str(j)
					if i != j and processedTransactions.index(j) < 0 and (self.transactions[i].intersectsWith(self.transactions[j]) or self.transactions[j].intersectsWith(self.transactions[i])):
						processedTransactions.append(j)
						transactionsThatIntersectWithI.append(self.transactions[j])
				transactionsWithIntersection.append(transactionsThatIntersectWithI)
		scheduleNumber = 1
		newSchedule = []
		for transactionList in transactionsWithIntersection:
			newSchedule.append('{} {} {} {}'.format(str(scheduleNumber), str(transactionList), ConflictResolver.testConflictSerializability(transactionList), ConflictResolver.testViewEquivalence(transactionList)))
			scheduleNumber += 1
		return newSchedule

	def __str__(self):
		representation = 'Schedule: ( transactions:\n'
		for key in self.transactions:
			representation += '  ' + str(self.transactions[key]) + '\n'
		representation += ' )'
		return representation

class Validator:
	@staticmethod
	def stringIsDigit(_str):
		if type(_str) == str and _str.isdigit():
			return True
		return False

	@staticmethod
	def stringIsOperation(_str):
		if type(_str) == str and len(_str) == 1 and _str.isalpha():
			return True
		return False

	@staticmethod
	def stringIsAttributeOrHyphen(_str):
		if type(_str) == str and len(_str) == 1 and (_str.isalpha() or _str == '-'):
			return True
		return False

	@staticmethod
	def validateOperation(op):
		if type(op) == list and len(op) == 4:
			if Validator.stringIsDigit(op[0]) and Validator.stringIsDigit(op[1]) and Validator.stringIsOperation(op[2]) and Validator.stringIsAttributeOrHyphen(op[3]):
				return True
		raise Exception('Operation ' + str(op) + ' is invalid')

	@staticmethod
	def isOperation(op):
		return type(op) == Operation

def main():
	schedule = Schedule()

	for line in sys.stdin:
		line = line.strip()
		if line != '':
			operationStr = line.split()
			Validator.validateOperation(operationStr)
			operation = Operation(operationStr[0], operationStr[1], operationStr[2].upper(), operationStr[3].upper())
			schedule.appendOperation(operation)

	scheduleWithoutConflicts = schedule.withoutConflicts()
	print('\n'.join(scheduleWithoutConflicts))

main()